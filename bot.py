from rivescript import RiveScript
from classifier import Classifier


class Bot:
    def __init__(self):
        self.bot = RiveScript()
        self.classifier = Classifier()

    def init_bot(self):
        self.bot.load_directory("./brain")
        self.bot.sort_replies()

    def bootstrap_classifier(self):
        self.classifier.init_stop_words()
        for line in open("./data/support.tsv"):
            intent, keywords = line.strip().split("\t")
            self.classifier.add_training_data(intent, keywords)
        self.classifier.init_training_data()

