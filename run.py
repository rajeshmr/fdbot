encoding='utf-8'
from sqlalchemy import create_engine
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import nltk.stem
import pandas as pd
import cPickle
import os
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from rivescript import RiveScript
from rivescript import sessions
from flask import Flask, request, redirect
from twilio.twiml.voice_response import VoiceResponse
import re
from vectorizers import StemmedTfidfVectorizer

def file_exists(file_path):
    return file_path and os.path.isfile(file_path)

english_stemmer = nltk.stem.SnowballStemmer('english')

class StoreBotNLP:

    def __init__(self):
        print('StoreBotNLP.__init__')
        self.ProductsToReturn=5
        self.intents_classifier_fn='intents_text_clf_mod.pkl'
        self.df_intents_fn='df_intents.pkl'
        self.prodloc_classifier_fn='prodloc_text_clf_mod.pkl'
        self.df_products_fn='df_products.pkl'
        self.LoadIntentsClassifier()
        self.LoadProdLocClassifier()

        self.bot = RiveScript(
##            debug=True,
            utf8=True)
        self.bot.load_directory(os.path.join(os.path.dirname(__file__), "brain"))
        self.bot.sort_replies()
        
    def TrainIntentsClassifier(self):
        target_column='intent'
        data_column='keywords'
        traincolumns=[target_column,data_column]
        data=[('greeting','hello hi howdy hey sup')
              ,('location','find need have where looking locate point want place aisle sell')
              ,('promotions','promotion promotions offer offers discount discounts coupon coupons save savings sale sales')
              ,('map','map schema')
              ,('help','help assist assistance lost option options')
              ,('departments','departments info')
              ,('associate','associate human operator person')
              ,('bye','bye goodbye later nite good night')
              ,('thanks','thank thanks thx thanx thks')
              ,('ok','ok k okay kk')
              ,('app','download app mobile')
              ,('list','list shopping')

              ]
        self.df_intents=pd.DataFrame(data=data,columns=traincolumns)

        intents_tfidf_vect=StemmedTfidfVectorizer()
        intents_knn_clf=KNeighborsClassifier(n_neighbors=1,weights='distance')
        self.intents_text_clf = Pipeline([
            ('vect',intents_tfidf_vect),
            ('knn', intents_knn_clf)])        
        self.intents_text_clf.fit(self.df_intents[data_column],self.df_intents[target_column])

    def TrainProdLocClassifier(self):
        target_column='location'
        data_column='name'
        engine = create_engine('mysql+pymysql://testing:1234@35.185.40.217/labrador?init_command=SET NAMES utf8 COLLATE utf8_unicode_ci', echo=True)

        self.df_products=pd.read_sql('select id,name,location from products', engine)
        products_tfidf_vect=StemmedTfidfVectorizer()
        prodloc_text_clf=KNeighborsClassifier(n_neighbors=5,weights='distance')
        self.prodloc_text_clf = Pipeline([
            ('vect',products_tfidf_vect),
            ('knn', prodloc_text_clf)])
        self.prodloc_text_clf.fit(self.df_products[data_column],self.df_products[target_column])

    def SaveIntentsClassifier(self):
        
        with open(self.intents_classifier_fn, 'wb') as fid:
            cPickle.dump(self.intents_text_clf, fid)
        with open(self.df_intents_fn, 'wb') as fid:
            cPickle.dump(self.df_intents, fid)



    def LoadIntentsClassifier(self):
        if file_exists(self.intents_classifier_fn):            
            with open(self.intents_classifier_fn, 'rb') as fid:
                self.intents_text_clf = cPickle.load(fid)
            with open(self.df_intents_fn, 'rb') as fid:
                self.df_intents = cPickle.load(fid)
        else:
            self.TrainIntentsClassifier()
            self.SaveIntentsClassifier()
            
    def SaveProdLocClassifier(self):
        
        with open(self.prodloc_classifier_fn, 'wb') as fid:
            cPickle.dump(self.prodloc_text_clf, fid)
            
        with open(self.df_products_fn, 'wb') as fid:
            cPickle.dump(self.df_products, fid)

    def LoadProdLocClassifier(self):
        if file_exists(self.prodloc_classifier_fn):            
            with open(self.prodloc_classifier_fn, 'rb') as fid:
                self.prodloc_text_clf = cPickle.load(fid)
            with open(self.df_products_fn, 'rb') as fid:
                self.df_products = cPickle.load(fid)
        else:
            self.TrainProdLocClassifier()
            self.SaveProdLocClassifier()

    def DetermineIntent(self,S):
        intent='wildcard'
        Y=self.intents_text_clf.named_steps['vect'].transform([S])
        n_dists,n_inds=self.intents_text_clf.named_steps['knn'].kneighbors(Y,1)
        dist=n_dists[0][0]
        ##when dist=1 it means that we did not find any vector, so it's wildcard
        if dist!=1:
            ind=n_inds[0][0]
            intent=self.df_intents.loc[ind]['intent']
        return intent

    def RemoveVerbsFromRequest(self,S):
        tokens=nltk.word_tokenize(S)
        tags=nltk.pos_tag(tokens)
##        print('Tags:{}'.format(tags))
        tags_to_use=[tag[0] for tag in tags if tag[1] in ['JJ','CD','NN','NNS']]
        tokens=[token for token in tokens if token in tags_to_use]
        S=' '.join(tokens)
        return S
        

    def GetNProducts(self,S,N):
        S=self.RemoveVerbsFromRequest(S)
        Y=self.prodloc_text_clf.named_steps['vect'].transform([S])
        n_dists,n_inds=self.prodloc_text_clf.named_steps['knn'].kneighbors(Y,N)
        inds=n_inds[0].tolist()
        prods=self.df_products.loc[inds]
        return S,prods

    def Test(self,S):
        print('='*25)
        print('Input:{}\nIntent:{}\nResults:\n{}'.format(S,self.DetermineIntent(S),self.DetermineLocation(S)))

     
    

    def Reply(self,from_number,message):        
        intent=self.DetermineIntent(message)
##        products=pd.DataFrame()
##        product_requested='unknown'
##        
##        if intent=='location':
##      we determine products always to pass it to rivescript, because there's WHERE topic
        product_requested,products=self.GetNProducts(message,self.ProductsToReturn)
            
        ##set bot user variables
        self.bot.set_uservar(from_number,'from_number',from_number)
        self.bot.set_uservar(from_number,'message',message)
        self.bot.set_uservar(from_number,'intent',intent)
        self.bot.set_uservar(from_number,'product_requested',product_requested)
        i=0
        for ind,product in products.iterrows():
            i=i+1           
            self.bot.set_uservar(from_number,'product{}'.format(i),product['name'])
            self.bot.set_uservar(from_number,'product{}_location'.format(i),product['location'])

        ##adjust message to trigger needed rivescript replies
        message='nlpintent is {} original message is {}'.format(intent,message)
        
        return self.bot.reply(from_number, message)

def get_NLP():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'nlp'):
        g.nlp = StoreBotNLP()
    return g.nlp

NLP = StoreBotNLP()
app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def sms():
    """Receive an inbound SMS and send a reply from RiveScript."""
##    print('sms')
    print(request)
    from_number = request.values.get("From", "unknown")
    message     = request.values.get("Body")
    reply       = "(Internal error)"

    print(request.values)
    print(message)

##    from_number='123'
##    message='where is fish'

##    NLP=get_NLP()
    # Get a reply from RiveScript.
    if message:
        reply = NLP.Reply(from_number, message)

    # Send the response.
    resp = VoiceResponse()

##  split reply into several messages by <delay seconds=N> using regex
    for sms in re.split(' \<delay seconds=[0-9]+\> ',reply):
        resp.sms(sms)

    resp_str=str(resp)
    print(resp_str)
    return resp_str

from twilio.twiml.messaging_response import MessagingResponse, Message
@app.route("/mms", methods=["GET", "POST"])
def mms():
    print request
    from_number = request.values.get("From", "unknown")
    message = request.values.get("Body")
    resp = MessagingResponse()
    if message:
        bot_reply = NLP.Reply(from_number, message)
        print bot_reply
        replies = re.split(r' \<delay seconds=[0-9]+\> ', bot_reply)
        for reply in replies:
            msg = Message()
            
            media_urls = re.findall(r'(https?://\S+[jpg|png|gif]$)', reply)
            reply = re.sub(r'(https?://\S+[jpg|png|gif]$)', '', reply, flags=re.MULTILINE)
            reply = reply.strip()
            
            if reply:
                msg.body(reply)

            if media_urls:
                for media_url in media_urls:
                    msg.media(media_url)
            resp.append(msg)         
    return str(resp)


##@app.teardown_appcontext
##def close_nlp(exception):
##    """Closes the database again at the end of the request."""
##    if hasattr(g, 'nlp'):
##        g.nlp.close()

if __name__ == "__main__":    
    app.run(
##        host='0.0.0.0',
##        debug=True,
##        port=12345,
##        use_reloader=True
        )
    
    
##  for local testing
    msg=''
    while msg != '/quit':
        msg = raw_input('You> ')
        reply = NLP.Reply("unknown",msg)
        print 'Bot>', reply

        
##    print(NLP.prodloc_text_clf.named_steps['vect'].transform(['no. 3/4 0%']))
##    print(NLP.prodloc_text_clf.named_steps['vect'].vocabulary_.get(u'100 %'))