from scrapy.spiders import SitemapSpider


class FreshdeskSupport(SitemapSpider):
    name = "scrape"
    domain = "https://support.freshdesk.com"
    sitemap_urls = ['https://support.freshdesk.com/support/sitemap.xml']
    sitemap_rules = [
        ('/support/', 'parse_support'),
    ]

    def parse_support(self, response):
        links = response.xpath("//a[@class='c-link']")
        for link in links:
            title = link.xpath("./text()").extract_first()
            link = link.xpath("./@href").extract_first()
            yield {"title": title, "link": self.domain + link}
