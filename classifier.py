import pandas as pd
import nltk.stem
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk.stem


# X = keywords, second col in data
# y = intent, first col in data

class Classifier:
    def __init__(self):
        self.stemmer = nltk.stem.SnowballStemmer('english')
        self.data = [
            ("greeting", "hello hi hey"),
            ('bye', 'bye goodbye later nite good night'),
            ('thanks', 'thank thanks thx thanx thks'),
            ('ok', 'ok k okay kk')
        ]
        self.intents = None
        self.knn = KNeighborsClassifier(n_neighbors=1, weights='distance')
        self.classifier = Pipeline([
            ('vect', TfidfVectorizer()),
            ('knn', self.knn)
        ])
        self.stop_words = []

        self.init_stop_words()

    def init_stop_words(self):
        print "initializing stop words"
        for word in open("./data/stopwords.txt"):
            self.stop_words.append(word.strip())

    def init_training_data(self):
        self.train()

    def add_training_data(self, intent, keywords):
        keywords = self.preprocess_keywords(keywords)
        self.data.append((intent, keywords))

    def add_training_data_iter(self, data):
        for intent, keywords in data:
            self.add_training_data(intent, keywords)

    def train(self):
        self.intents = pd.DataFrame(data=self.data, columns=["intent", "keywords"])
        self.classifier.fit(self.intents["keywords"], self.intents["intent"])

    def predict(self, text):
        intent = "*"
        text = self.preprocess_keywords(text)
        y = self.classifier.named_steps['vect'].transform([text])
        distance, inferred_intent = self.classifier.named_steps['knn'].kneighbors(y, 1)
        print distance, inferred_intent
        distance = distance[0][0]
        inferred_intent_index = inferred_intent[0][0]
        if distance != 1:
            intent = self.intents.loc[inferred_intent_index]['intent']
        return intent

    def preprocess_keywords(self, keywords):
        keywords = keywords.lower()
        tokens = nltk.word_tokenize(keywords)
        tokens = filter(lambda x: x not in self.stop_words, tokens)
        stemmed_tokens = [self.stemmer.stem(token) for token in tokens]
        print stemmed_tokens
        return " ".join(stemmed_tokens)


if __name__ == '__main__':
    t = Classifier()
    t.init_training_data()
    print t.predict("hello")

    for line in open("./data/support.tsv"):
        intent, keywords = line.strip().split("\t")
        t.add_training_data(intent, keywords)
    t.train()
    while True:
        user_input = raw_input("input> ")
        print t.predict(user_input)